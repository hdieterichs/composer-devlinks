<?php

namespace Hediet\Composer\DevLinks;

use Hediet\Composer\DevLinks\DevLinksConfig;

if (!function_exists("Hediet\\Composer\\DevLinks\\isRelativePath"))
{
    function isRelativePath($path)
    {
        return preg_match("/^((\\/)|([A-Za-z]\\:))/", $path) === 0;
    }
}

$composerJsonDir = realpath(dirname(__FILE__) . "/../../../../") . "/";
$composerLinksDevJson = $composerJsonDir . "composer.links.dev.json";

if (file_exists($composerLinksDevJson))
{
    $config = DevLinksConfig::loadFromFile($composerLinksDevJson);
    
    foreach ($config->getIncludedFiles() as $file)
    {
        if (isRelativePath($file))
        {
            $file = $composerJsonDir . $file;
        }
        
        require_once($file);
    }
}